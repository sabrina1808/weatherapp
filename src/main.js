import Vue from 'vue'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {faCloud, faClock, faMapMarker, faTachometerAlt, faThermometerHalf, faTint, faWind} from '@fortawesome/free-solid-svg-icons'

Vue.config.productionTip = false

library.add(faCloud, faClock, faMapMarker, faTachometerAlt, faThermometerHalf, faTint, faWind)

Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  render: h => h(App),
}).$mount('#app')